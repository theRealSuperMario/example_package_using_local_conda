```bash
conda search -c file:///Users/sandrobraun/Projekte/gitlab_projects/example_conda_package/conda_build/conda-bld/ --override-channels


conda install -c file:///Users/sandrobraun/Projekte/gitlab_projects/example_conda_package/conda_build/conda-bld -c conda-forge ml
python -c "import package; print(package.__version__)"
```

Use the project [here](https://gitlab.com/theRealSuperMario/example_conda_package) as a dependency for this project.

- See https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/create-custom-channels.html